<?php
declare(strict_types=1);

namespace Lendable\Interview\Interpolation\Repository;

use Lendable\Interview\Interpolation\Model\LoanApplication;
use Lendable\Interview\Interpolation\Service\Fee\Dto\Fee;
use Lendable\Interview\Interpolation\Service\Fee\Dto\FeeBoundaries;
use Lendable\Interview\Interpolation\Service\Fee\Exception\FeeBoundariesNotFound;
use Lendable\Interview\Interpolation\Service\Fee\FeeRepositoryInterface;

class FeeRepository implements FeeRepositoryInterface
{
    const TERMS = [
        12 => [
            [1000, 50],
            [2000, 90],
            [3000, 90],
            [4000, 115],
            [5000, 100],
            [6000, 120],
            [7000, 140],
            [8000, 160],
            [9000, 180],
            [10000, 200],
            [11000, 220],
            [12000, 240],
            [13000, 260],
            [14000, 280],
            [15000, 300],
            [16000, 320],
            [17000, 340],
            [18000, 360],
            [19000, 380],
            [20000, 400],
        ],
        24 => [
            [1000, 70],
            [2000, 100],
            [3000, 120],
            [4000, 160],
            [5000, 200],
            [6000, 240],
            [7000, 280],
            [8000, 320],
            [9000, 360],
            [10000, 400],
            [11000, 440],
            [12000, 480],
            [13000, 520],
            [14000, 560],
            [15000, 600],
            [16000, 640],
            [17000, 680],
            [18000, 720],
            [19000, 760],
            [20000, 800],
        ],
    ];

    /**
     * @inheritdoc
     * @throws FeeBoundariesNotFound
     */
    public function getFeeBoundaries(LoanApplication $loan): FeeBoundaries
    {
        $terms = self::TERMS[$loan->getTerm()] ?? [];
        $loanAmount = $loan->getAmount();

        for ($i = 0; $i < count($terms) - 1; $i++) {
            list($lowerAmount, $lowerFee) = $terms[$i];
            list($upperAmount, $upperFee) = $terms[$i + 1];

            if ($loanAmount >= $lowerAmount && $loanAmount <= $upperAmount) {
                return new FeeBoundaries(
                    new Fee($lowerAmount, $lowerFee),
                    new Fee($upperAmount, $upperFee)
                );
            }
        }

        throw new FeeBoundariesNotFound(
            sprintf(
                'Fee Boundaries not found for £%s loan amount and term %s months',
                $loan->getAmount(),
                $loan->getTerm()
            )
        );
    }
}
