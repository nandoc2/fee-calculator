<?php
declare(strict_types=1);

namespace Lendable\Interview\Interpolation\Service\Fee\Exception;

use Exception;

class FeeBoundariesNotFound extends Exception
{
}
