<?php
declare(strict_types=1);

namespace Lendable\Interview\Interpolation\Service\Fee\Dto;

class Fee
{
    /** @var float */
    private $loanAmount;

    /** @var float */
    private $fee;

    public function __construct(float $loanAmount, float $fee)
    {
        $this->loanAmount = $loanAmount;
        $this->fee = $fee;
    }

    public function getLoanAmount(): float
    {
        return $this->loanAmount;
    }

    public function getFee(): float
    {
        return $this->fee;
    }
}
