<?php
declare(strict_types=1);

namespace Lendable\Interview\Interpolation\Service\Fee\Dto;

class FeeBoundaries
{
    /** @var Fee */
    private $lowerFee;

    /** @var Fee */
    private $upperFee;

    public function __construct(Fee $lowerFee, Fee $upperFee)
    {
        $this->lowerFee = $lowerFee;
        $this->upperFee = $upperFee;
    }

    public function getLowerFee(): Fee
    {
        return $this->lowerFee;
    }

    public function getUpperFee(): Fee
    {
        return $this->upperFee;
    }
}
