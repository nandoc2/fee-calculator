<?php
declare(strict_types=1);

namespace Lendable\Interview\Interpolation\Service\Fee;

use Lendable\Interview\Interpolation\Model\LoanApplication;
use Lendable\Interview\Interpolation\Service\Fee\Dto\FeeBoundaries;
use Lendable\Interview\Interpolation\Service\Fee\Exception\FeeBoundariesNotFound;

interface FeeRepositoryInterface
{
    /**
     * @param LoanApplication $loan
     * @return FeeBoundaries
     * @throws FeeBoundariesNotFound
     */
    public function getFeeBoundaries(LoanApplication $loan): FeeBoundaries;
}
