<?php
declare(strict_types=1);

namespace Lendable\Interview\Interpolation\Service\Fee;

use Lendable\Interview\Interpolation\Model\LoanApplication;
use Lendable\Interview\Interpolation\Service\Fee\Dto\FeeBoundaries;

class LinearInterpolatorFeeCalculator implements FeeCalculatorInterface
{
    const FEE_MOD = 5;

    /** @var FeeRepositoryInterface */
    private $feeRepository;

    public function __construct(FeeRepositoryInterface $feeRepository)
    {
        $this->feeRepository = $feeRepository;
    }

    /**
     * @inheritdoc
     * @throws Exception\FeeBoundariesNotFound
     */
    public function calculate(LoanApplication $application): float
    {
        $feeBoundaries = $this->feeRepository->getFeeBoundaries($application);
        $feeAmount = $this->calculateLinearInterpolationFee($application->getAmount(), $feeBoundaries);

        return $this->roundUpFeeAmount($feeAmount, $application->getAmount());
    }

    private function calculateLinearInterpolationFee(float $loanAmount, FeeBoundaries $feeBoundaries): float
    {
        $x0 = $feeBoundaries->getLowerFee()->getLoanAmount();
        $x1 = $feeBoundaries->getUpperFee()->getLoanAmount();
        $x2 = $loanAmount;

        $y0 = $feeBoundaries->getLowerFee()->getFee();
        $y1 = $feeBoundaries->getUpperFee()->getFee();

        // y2
        return (($x1 - $x2) * $y0 + ($x2 - $x0) * $y1) / ($x1 - $x0);
    }

    private function roundUpFeeAmount(float $feeAmount, float $loanAmount): float
    {
        $sum = $feeAmount + $loanAmount;
        $roundedSum = $this->roundUpAmountByModulo($sum, self::FEE_MOD);

        return round($feeAmount + ($roundedSum - $sum), 2);
    }

    private function roundUpAmountByModulo(float $amount, int $mod): float
    {
        $amount = ceil($amount);

        return $amount % $mod > 0
            ? ($amount + $mod) - ($amount % $mod)
            : $amount;
    }
}
