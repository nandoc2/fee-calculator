<?php
declare(strict_types=1);

namespace Lendable\Interview\Interpolation\Tests\Service\Fee;

use Lendable\Interview\Interpolation\Model\LoanApplication;
use Lendable\Interview\Interpolation\Service\Fee\Dto\Fee;
use Lendable\Interview\Interpolation\Service\Fee\Dto\FeeBoundaries;
use Lendable\Interview\Interpolation\Service\Fee\FeeCalculatorInterface;
use Lendable\Interview\Interpolation\Service\Fee\FeeRepositoryInterface;
use Lendable\Interview\Interpolation\Service\Fee\LinearInterpolatorFeeCalculator;
use PHPUnit\Framework\TestCase;
use Prophecy\Prophecy\ObjectProphecy;

class LinearInterpolatorFeeCalculatorTest extends TestCase
{
    /** @var FeeRepositoryInterface|ObjectProphecy */
    private $feeRepository;

    /** @var FeeCalculatorInterface */
    private $feeCalculator;

    public function setUp()
    {
        $this->feeRepository = $this->prophesize(FeeRepositoryInterface::class);
        $this->feeCalculator = new LinearInterpolatorFeeCalculator($this->feeRepository->reveal());
    }

    /**
     * @test
     * @dataProvider provideTestData
     *
     * @param float $loanAmount
     * @param float[] $lowerFee
     * @param float[] $upperFee
     * @param float $expectedFee
     */
    public function itCalculatesFee(float $loanAmount, array $lowerFee, array $upperFee, float $expectedFee): void
    {
        $loan = new LoanApplication(12, $loanAmount);

        $boundaries = new FeeBoundaries(
            new Fee($lowerFee[0], $lowerFee[1]),
            new Fee($upperFee[0], $upperFee[1])
        );

        $this->feeRepository
            ->getFeeBoundaries($loan)
            ->willReturn($boundaries);

        $fee = $this->feeCalculator->calculate($loan);

        self::assertSame($expectedFee, $fee);
        self::assertSame(0, ($fee + $loanAmount) % 5);
    }

    public function provideTestData(): array
    {
        return [
            [
                2750,        // loan amount
                [2000, 100], // lower loan amount, fee
                [3000, 120], // upper loan amount, fee
                115.0        // expected fee
            ],
            [
                1000,
                [1000, 100],
                [2000, 120],
                100.0
            ],
            [
                2000,
                [1000, 100],
                [2000, 120],
                120.0
            ],
            [
                2750.40,
                [2000, 100],
                [3000, 120],
                119.6
            ],
            [
                10965.12,
                [10000, 500],
                [11000, 600],
                599.88
            ],
            [
                21000.33,
                [20000, 500],
                [21000.33, 600.33],
                604.67
            ],
        ];
    }
}
