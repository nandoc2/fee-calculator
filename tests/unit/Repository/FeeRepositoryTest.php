<?php
declare(strict_types=1);

namespace Lendable\Interview\Interpolation\Tests\Repository;

use Lendable\Interview\Interpolation\Model\LoanApplication;
use Lendable\Interview\Interpolation\Repository\FeeRepository;
use Lendable\Interview\Interpolation\Service\Fee\FeeRepositoryInterface;
use PHPUnit\Framework\TestCase;

class FeeRepositoryTest extends TestCase
{
    /** @var FeeRepositoryInterface */
    private $repository;

    public function setUp()
    {
        $this->repository = new FeeRepository();
    }

    /**
     * @test
     * @dataProvider provideTestData
     *
     * @param mixed[] $loan
     * @param int[] $expectedLower
     * @param int[] $expectedUpper
     */
    public function itGetsFeeBoundaries(array $loan, array $expectedLower, array $expectedUpper): void
    {
        $boundaries = $this->repository->getFeeBoundaries(new LoanApplication($loan[0], $loan[1]));

        self::assertSame($expectedLower[0], (int) $boundaries->getLowerFee()->getLoanAmount());
        self::assertSame($expectedLower[1], (int) $boundaries->getLowerFee()->getFee());

        self::assertSame($expectedUpper[0], (int) $boundaries->getUpperFee()->getLoanAmount());
        self::assertSame($expectedUpper[1], (int) $boundaries->getUpperFee()->getFee());
    }

    /**
     * @test
     * @expectedException \Lendable\Interview\Interpolation\Service\Fee\Exception\FeeBoundariesNotFound
     */
    public function itThrowsForUnexpectedTerm(): void
    {
        $this->repository->getFeeBoundaries(new LoanApplication(30, 100));
    }

    /**
     * @test
     * @expectedException \Lendable\Interview\Interpolation\Service\Fee\Exception\FeeBoundariesNotFound
     */
    public function itThrowsForUnexpectedLoanAmount(): void
    {
        $this->repository->getFeeBoundaries(new LoanApplication(24, 1));
    }

    public function provideTestData(): array
    {
        return [
            [
                [12, 1000.1], // loan term, amount
                [1000, 50],   // expected lower loan amount, fee
                [2000, 90],   // expected upper loan amount, fee
            ],
            [
                [12, 2000.99],
                [2000, 90],
                [3000, 90],
            ],
            [
                [12, 20000],
                [19000, 380],
                [20000, 400],
            ],
            [
                [24, 1000],
                [1000, 70],
                [2000, 100],
            ],
            [
                [24, 5678],
                [5000, 200],
                [6000, 240],
            ],
        ];
    }
}
